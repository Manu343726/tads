﻿/************************************************************************
* _LinkedContainer.h - Manuel Sánchez Pérez	- Mar 2013					*
*																		*
* La plantilla _LinkedContainer<T> provee la implementación completa de *
* una lista doblemente enlazada, para ser utilizada en                  *
* diferentes TADs.                                                      *
************************************************************************/

#ifndef _LinkedContainer_H
#define _LinkedContainer_H

#include "Base.h"

template<class T>
class _LinkedContainer
{
protected:
	class _LinkedListNode
	{
	public:
		T _data;
		_LinkedListNode* _next;
		_LinkedListNode* _previous;

		_LinkedListNode()                                                              : _next(NULL)                                     {}
		_LinkedListNode(const T& data)                                                 : _next(NULL) , _data(data)                       {}
		_LinkedListNode(const T& data,_LinkedListNode* previous,_LinkedListNode* next) : _next(next) , _previous(previous) , _data(data) {}
		_LinkedListNode(_LinkedListNode* previous)                                     : _next(NULL) , _previous(previous)               {} //Para el constructor del iterador end()
	};

	_LinkedListNode* _begin;
	_LinkedListNode* _end;
	uint _count;

	_LinkedContainer() : _begin(NULL) , _end(NULL) , _count(0) {}
	~_LinkedContainer()
	{
		_clear();
	}

	void _clear()
	{
		if(_count > 0)
		{
			_LinkedListNode* next = _begin->_next;

			while(next != _end->_next)
			{
				delete _begin;
				_begin = next;
				next = next->_next;
			}

			_begin = NULL;
			_end = NULL;
			_count = 0;
		}
	}

	void _push_front(const T& data)
	{
		_LinkedListNode* newNode = new _LinkedListNode(data,NULL,_begin);

		if(_begin != NULL)
			_begin->_previous = newNode;

		_begin = newNode;

		if(_end == NULL)
			_end = _begin;

		_count++;
	}

	void _push_back(const T& data)
	{
		_LinkedListNode* newNode = new _LinkedListNode(data,_end,NULL);

		if(_end != NULL)
			_end->_next = newNode;

		_end = newNode;

		if(_begin == NULL)
			_begin = _end;

		_count++;
	}

	void _pop_front()
	{
		if(_begin != NULL)
		{
			_LinkedListNode* newBegin = _begin->_next;

			if(newBegin != NULL)
				newBegin->_previous = NULL;

			delete _begin;

			_begin = newBegin;
			if(_end == NULL)
				_begin = NULL;
			_count--;
		}
	}

	void _pop_back()
	{
		if(_end != NULL)
		{
			_LinkedListNode* newEnd = _end->_previous;

			if(newEnd != NULL)
				newEnd->_next = NULL;

			delete _end;

			_end = newEnd;
			if(_end == NULL)
				_begin = NULL;

			_count--;
		}
	}

	void _insert(const T& data, _LinkedListNode* node)
	{
		if(node == _begin)
			_push_front(data);
		else if(node == _end)
			_push_back(data);
		else
		{
			_stc_insert(data,node);
			_count++;
		}
	}

	void _erase(_LinkedListNode * node)
	{
		if(node == _begin)
			_pop_front();
		else if(node == _end)
			_pop_back();
		else
		{
			_stc_erase(node);
			_count--;
		}
	}

	const uint _Size()    const {return _count;} //Ojo, es el tamaño del contenedor, no de su implementación (Al usuario no le interesa como lo has implementado)
	const bool _isEmpty() const {return _begin == NULL;}
private:
	static _LinkedListNode* _stc_insert(const T& data, _LinkedListNode* node)
	{
		_LinkedListNode* newNode = new _LinkedListNode(data,node->_previous,node);

		if(node->_previous != NULL)
			node->_previous->_next = newNode;
		if(node->_next != NULL)
			node->_next->_previous = newNode;

		return newNode;
	}

	static void _stc_erase(_LinkedListNode* node) throw(InvalidTADOperationException)
	{
		if(node != NULL)
		{
			_LinkedListNode* next = node->_next;
			_LinkedListNode* previous = node->_previous;

			if(next != NULL)
				next->_previous = previous;
			if(previous != NULL)
				previous->_next = next;

			delete node;
		}
		else
			throw InvalidTADOperationException();
	}
};
#endif