/**************************************************************
* Stack.h - Manuel Sánchez Pérez - Mar 2013	            	  *
*														      *
* Implementación de pila mediante un array dinámico           *
*												     	      *
* NOTA: La implementación es idéntica a la de LinkedStack<T>, *
*  únicamente se diferencian por el contenedor utilizado.     *
**************************************************************/

#ifndef STACK_H
#define STACK_H

#include "_LinearContainer.h"

template<class T>
class Stack : public _LinearContainer<T>
{
public:
	void push_back(const T& data)  {_push_back(data);}
	void pop_back()   {_pop_back();}

	T& back() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _end->_data;
		else
			throw InvalidTADOperationException("Stack<T>::back() : The stack is empty");
	}

	const uint size()    const {return _Size();}
	const bool isEmpty() const {return _isEmpty();}
	void clear()               {_clear();}
};

#endif