/*******************************************************************
* LinkedStack.h - Manuel Sánchez Pérez - Mar 2013		   *
*								   *
* Implementación de pila mediante una lista doblemente enlazada    *
*								   *
* NOTA: La implementación es idéntica a la de Stack<T>, únicamente *
* se diferencian por el contenedor utilizado.                      *
*******************************************************************/

#ifndef LINKED_STACK_H
#define LINKED_STACK_H

#include "_LinkedContainer.h"

template<class T>
class LinkedStack : public _LinkedContainer<T>
{
public:
	void push_back(const T& data)  {_push_back(data);}
	void pop_back()                {_pop_back();}

	T& back() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _end->_data;
		else
			throw InvalidTADOperationException("LinkedStack<T>::back() : The stack is empty");
	}

	const uint size()    const {return _Size();}
	const bool isEmpty() const {return _isEmpty();}
	void clear()               {_clear();}
};

#endif