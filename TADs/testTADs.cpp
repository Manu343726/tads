#include <iostream>
#include <string>
#include "Array.h"
#include "List.h"
#include "LinkedList.h"
#include "Stack.h"
#include "LinkedStack.h"
#include "LinkedQueue.h"

using namespace std;

const int LISTSIZE = 10;


//Una consulta de ejemplo: Congruencia
class Congruence : public Query<int>
{
private:
	int _module;
public:
	Congruence(int module) : _module(module) {}

	bool operator()(const int& data) {return data % _module == 0;}
};

//Otro ejemplo: String::contains()
class Matcher : public Query<string>
{
private:
	string _match;
public:
	Matcher(string match) : _match(match) {}
	bool operator()(const string& data) {return data.find(_match) != string::npos;}
};

int main()
{
	List<int> myList;
	LinkedList<int> myLinkedList;
	//Stack<int> myStack;
	//LinkedStack<int> myLinkedStack;
	LinkedQueue<int> myLinkedQueue;
	Congruence myQuery(10);
	Matcher myMatcher("Manu");
	List<string> myStringList;

	myStringList.push_back("Hola Miguel que tal?");
	myStringList.push_back("Hola Laura que tal?");
	myStringList.push_back("Hola Monica que tal?");
	myStringList.push_back("Hola Manu que tal?");
	myStringList.push_back("Hola Lider que tal?");
	myStringList.push_back("Hola que tal?");
	myStringList.push_back("Hola que tal?");
	myStringList.push_back("Hola que tal?");
	myStringList.push_back("Hola Manu que tal?");

	try
	{
		for(uint i = 0; i<LISTSIZE;++i)
		{
			myList.push_back(i);
			myLinkedList.push_back(i);
			myLinkedQueue.push_front(i);
		}

		cout << "List<string> query:" << endl;
		for(auto it = myStringList.beginQuery(&myMatcher); it != myStringList.endQuery();++it)
			cout << *it << endl;

		cout << "List query:" << endl;
		for(List<int>::ListQueryIterator it = myList.beginQuery(&myQuery); it != myList.endQuery(); ++it)
			cout << *it << endl;

		cout << "List:" << endl;
		for(List<int>::ListIterator it = myList.begin(); it != myList.end(); ++it)
			cout << *it << endl;

		cout << "LinkedList query:" << endl;
		for(LinkedList<int>::LinkedListQueryIterator it = myLinkedList.beginQuery(&myQuery); it != myLinkedList.endQuery(); ++it)
			cout << *it << endl;

		cout << "LinkedList:" << endl;
		for(LinkedList<int>::LinkedListIterator it = myLinkedList.begin(); it != myLinkedList.end(); ++it)
			cout << *it << endl;

		cout << "LinkedQueue:" << endl;
		while(!myLinkedQueue.isEmpty())
		{
			cout << "Front: " << myLinkedQueue.front() << " Back: " << myLinkedQueue.back() << endl;
			myLinkedQueue.pop_back();
		}

	}
	catch(TADException ex)
	{
		cout << ex.what() << endl;
	}

	cin.sync();
	cin.get();
}