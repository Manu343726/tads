/************************************************************************
* _LinearContainer.h - Manuel Sánchez Pérez	- Mar 2013					*
*																		*
* La plantilla _LinkedContainer<T> provee la implementación completa de *
* un array dinámico, para ser utilizado en diferentes TADs.             *
************************************************************************/

#ifndef LINEARCONTAINER_H
#define LINEARCONTAINER_H
#include "Base.h"

const int INITALLOCSIZE = 10;
const float EXPANDFACTOR = 2.0f; 
const float CONTRACTFACTOR = 0.5f;

//Contenedor secuencial de datos:
template<class T>
class _LinearContainer
{
protected:
	T* _array;
	uint _size;
	uint _count;
	
	void _resize(uint newSize)
	{
		uint minSize = _count < newSize ? _count : newSize;
		T* newArray = new T[newSize];

		for(uint i=0;i<minSize;++i)
			newArray[i] = _array[i];

		delete [] _array;
		_array = newArray;
		_size = newSize;
	}

	void _clear()
	{
		_resize(INITALLOCSIZE);
		_count = 0;
	}

	void _rightShift(uint index)
	{
		for(uint i= _count;i>index;--i)
			_array[i] = _array[i-1];
	}

	void _leftShift(uint index)
	{
		for(uint i = index;i<_count - 1; ++i)
			_array[i] = _array[i+1];
	}

	void _insert(const T& value, uint index) throw(OutOfRangeException)
	{
		if(index <= _count)
		{
			if(_count == _size)
				_resize((uint)(_size*EXPANDFACTOR));

			_rightShift(index);
			_array[index] = value;
			_count++;
		}
		else
			throw OutOfRangeException(_count+1,index);
	}

	void _erase(uint index) throw(OutOfRangeException)
	{
		if(index < _count)
		{
			if(_size > INITALLOCSIZE && _count <= (uint)(_size*CONTRACTFACTOR))
				_resize((uint)(_size*CONTRACTFACTOR));

			_leftShift(index);
			_count--;
		}
		else
			throw OutOfRangeException(_count,index);
	}

	void _push_back(const T& value)  throw(OutOfRangeException) {return _insert(value,_count);}
	void _push_front(const T& value) throw(OutOfRangeException) {return _insert(value,0);}
	void _pop_back()                 throw(OutOfRangeException) {return _erase(_count-1);}
	void _pop_front()                throw(OutOfRangeException) {return _erase(0);}

	const uint _Size()    const {return _count;} //Ojo, es el tamaño del contenedor, no de su implementación (Al usuario no le interesa como lo has implementado)
	const bool _isEmpty() const {return _count == 0;}


	//Constructor protected: Ésta clase solo es "instanciable" por sus clases derivadas, el usuario no puede utilizarla 
	_LinearContainer()
	{
		_array = new T[INITALLOCSIZE];
		_count = 0;
		_size = INITALLOCSIZE;
	}

	~_LinearContainer()
	{
		SAFEDELETE_ARRAY(_array);
	}

	_LinearContainer(const _LinearContainer<T>& other) = delete;
};
#endif