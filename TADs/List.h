/*****************************************************
* List.h - Manuel Sánchez Pérez - Mar 2013	     *
*						     *
* Implementación de lista mediante un array dinámico *
*****************************************************/

#ifndef LIST_H
#define LIST_H

#include "_LinearContainer.h"
#include "Iterator.h"

template<class T>
class List : public _LinearContainer<T>
{
	friend class ListIterator;
public:
	class ListIterator : public BidirectionalIterator<T>
	{
		friend class List<T>;
	private:
		uint _index;
		T* _containerArray;
		uint _containerSize;

		ListIterator(uint index,uint size,T* array) {_index = index;_containerSize = size; _containerArray = array;}
	public:
		void operator++() {_index++;}
		void operator--() {_index--;}
		void operator+=(uint offset) {_index+=offset;}
		void operator-=(uint offset) {_index-=offset;}

		ListIterator operator+(uint offset) const {return ListIterator(_index + offset,_containerSize,_containerArray);} //No es necesario retorno por referencia (RVO)
		ListIterator operator-(uint offset) const {return ListIterator(_index - offset,_containerSize,_containerArray);} //No es necesario retorno por referencia (RVO)

		T& at() throw(OutOfRangeException)
		{
			if(_index<_containerSize)
				return _containerArray[_index];
			else
				throw OutOfRangeException(_containerSize,_index,"ListIterator::at() : Invalid iterator");
		}

		bool operator==(const ListIterator& other) const {return _index == other._index && _containerArray == other._containerArray;}
		bool operator!=(const ListIterator& other) const {return !(*this == other);}

		bool operator>(const ListIterator& other) throw(IncomparableException)
		{
			if(_containerArray == other._containerArray)
				return _index > other._index;
			else
				throw IncomparableException("ListIterator::operator>() : Iterators not come from the same container");
		}
		bool operator>=(const ListIterator& other) throw(IncomparableException)
		{
			if(_containerArray == other._containerArray)
				return _index >= other._index;
			else
				throw IncomparableException("ListIterator::operator>=() : Iterators not come from the same container");
		}
		bool operator<(const ListIterator& other) throw(IncomparableException)
		{
			if(_containerArray == other._containerArray)
				return _index < other._index;
			else
				throw IncomparableException("ListIterator::operator<() : Iterators not come from the same container");
		}
		bool operator<=(const ListIterator& other) throw(IncomparableException)
		{
			if(_containerArray == other._containerArray)
				return _index <= other._index;
			else
				throw IncomparableException("ListIterator::operator<=() : Iterators not come from the same container");
		}
	};

	class ListQueryIterator : public QueryIterator<T>
	{
		friend class List<T>;
	private:
		uint _index;
		T* _containerArray;
		uint _containerSize;
	
		ListQueryIterator(Query<T>* Query, uint index, uint containerSize, T* containerArray) : QueryIterator<T>(Query) , _index(index) , _containerArray(containerArray) , _containerSize(containerSize) {}
	public:
		T& at() throw(OutOfRangeException)
		{
			if(_index<_containerSize)
				return _containerArray[_index];
			else
				throw OutOfRangeException(_containerSize,_index,"ListIterator::at() : Invalid iterator");
		}

		void operator++() throw(OutOfRangeException)
		{
			if(_query != NULL)
			{
				_index++;

				while(_index < _containerSize && !(*_query)(at()))
					_index++;
			}
		}

		bool operator==(const ListQueryIterator& other) const {return _index == other._index && _containerArray == other._containerArray;}
		bool operator!=(const ListQueryIterator& other) const {return !(*this == other);}
	};

	List(){};//Lo hace todo la clase base
	~List(){};//Lo hace todo la clase base

	List(const List<T>& other)
	{

	}

	void insert(const T& value, uint index)              throw(OutOfRangeException) {_insert(value,index);}
	void insert(const T& value,const ListIterator& iter) throw(OutOfRangeException) {_insert(value,iter._index);}
	void erase(uint index)                               throw(OutOfRangeException) {_insert(index);}
	void erase(const ListIterator& iter)                 throw(OutOfRangeException) {_erase(iter._index);}
	void push_back(const T& value)                       throw(OutOfRangeException) {_push_back(value);}
	void pop_back()                                      throw(OutOfRangeException) {_pop_back();}

	const uint size()    const {return _Size();}
	const bool isEmpty() const {return _isEmpty();}
	void clear()               {_clear();}

	const ListIterator begin() {return ListIterator(0,_count,_array);} //No hace falta que el retorno sea por referencia (RVO)
	const ListIterator end()   {return ListIterator(_count,_count,_array);} //No hace falta que el retorno sea por referencia (RVO)

	const ListQueryIterator beginQuery(Query<T>* Query) {return ListQueryIterator(Query,0,_count,_array);}
	const ListQueryIterator endQuery()                  {return ListQueryIterator(NULL,_count,_count,_array);}

	T& at(uint index) throw(OutOfRangeException)
	{
		if(index < _count)
			return _array[index];
		else
			throw OutOfRangeException(_count,index,"List<T>::at() : 'index' is out of range");
	}
	T& operator[](uint index) throw(OutOfRangeException) {return at(index);}

	T& front() throw(OutOfRangeException) {return at(0);}
	T& back()  throw(OutOfRangeException) {return at(_count-1);}
};
#endif