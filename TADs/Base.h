/*******************************************************************************************************************************************
* Base.h - Manuel Sánchez Pérez	- Mar 2013												   *
*															                   *
* Éste archivo contiene las definiciones de los tipos básicos utilizados por los TADs, así como algunas herramientas.	                   *
*                                                                                                                                          *
* La sección "Herramientas varias" está sacada del archivo de configuración del proyecto dx_lib32 (https://github.com/Manu343726/dx_lib32) *
*******************************************************************************************************************************************/

#ifndef BASE_H
#define BASE_H

#ifdef _MSC_VER
#pragma warning(disable : 4290) //Visual Studio no implementa la especificación de excepciones. A cambio te lanza el warning 4290 (http://msdn.microsoft.com/en-us/library/sa28fef8.aspx)
#endif

#include <exception>
using namespace std;

#ifndef uint
typedef unsigned int uint; //Más sencillo de escribir...
#endif
//Definimos un tipo "rango", que utilizaremos para guardar información sobre buffer overflows, entre otras cosas:
struct Range
{
private:
	uint _begin;
	uint _end;
public:
	//CONSTRUCTORES:
	Range()                     {_begin=0;_end=0;}
	Range(uint begin, uint end) {_begin = begin; _end = end;}
	Range(uint size)            {_begin=0;_end = size-1;}

	//OBSERVADORES:
	uint begin()              const {return _begin;}
	uint end()                const {return _end;}
	bool contains(uint value) const {return _begin <= value && value <= _end;}

	//OPERADORES:
	bool operator==(Range other) {return _begin == other._begin && _end == other._end;}
	bool operator!=(Range other) {return !(*this == other);}
};

#define EXCEPTION_DEFAULT_MESSAGE(className) "Unexpected " #className

//Debería ser ADTException, pero lo dejo así para seguir la notación de la asignatura:
class TADException : exception
{
private: 
    const char* _message;
public:
    TADException() {}
    TADException(const char* message) : _message(message) {}
    
    const char* what() {return _message;}
};
typedef TADException InvalidTADOperationException;
typedef InvalidTADOperationException IncomparableException;


//Out of range exception:
class OutOfRangeException : public InvalidTADOperationException
{
private:
	Range _range;
	uint _value;
public:
	OutOfRangeException(Range range, uint value, char* message = EXCEPTION_DEFAULT_MESSAGE(OutOfRangeException)) : InvalidTADOperationException(message) {_range = range; _value = value;}
	OutOfRangeException(uint size, uint value, char* message = EXCEPTION_DEFAULT_MESSAGE(OutOfRangeException))   : InvalidTADOperationException(message) , _range(size) {_value = value;}

	const uint value()  const {return _value;}
	const Range range() const {return _range;}
};

/******************************
*     HERRAMIENTAS VARIAS     *
******************************/

#define SAFEDELETE_POINTER(x) {delete (x); (x)=NULL;}
#define SAFEDELETE_ARRAY(x) {delete[] (x); (x)=NULL;}


//Es una parida, lo se, pero para configurar a base de preprocesador es mas cómodo (El código queda mas claro):
#ifndef TRUE
	#define TRUE 1
	#define FALSE 0
#endif

#define ANDOP(a,b) ((a == TRUE && b == TRUE) ? TRUE : FALSE) 
#define OROP(a,b) ((a == TRUE || b == TRUE) ? TRUE : FALSE) 
#define NOTOP(a) ((a == TRUE) ? FALSE : TRUE)

#ifndef YES
	#define YES TRUE
	#define NO FALSE
#endif
#ifndef ENABLED
	#define ENABLED TRUE
	#define DISABLED FALSE
#endif

//Por si se da el caso de usar floats: 
#define FLOAT_EPSILON 0.0001

#define FLOAT_EQ(x,y) (((y - FLOAT_EPSILON) < x) && ((y + FLOAT_EPSILON) > x))
#define FLOAT_NE(x,y) !FLOAT_EQ(x,y)
#define FLOAT_EQCERO(x) ((x < FLOAT_EPSILON) && (x > -FLOAT_EPSILON))
#define FLOAT_NECERO(x) !FLOAT_EQCERO(x)

#define FLOAT_GE(x,y) (FLOAT_EQ(x,y) || (x > y))
#define FLOAT_LE(x,y) (FLOAT_EQ(x,y) || (x < y))
#endif