/*****************************************************************************************************************************
* Iterator.h - Manuel Sánchez Pérez - Mar 2013							  		             *
*									         					     *
* Éste archivo contiene las definiciones de las diferentes categorías de iteradores.                                         *
* Es una versión simplificada de la clasificación de iteradores de la STL (http://www.cplusplus.com/reference/iterator/).    *
* Únicamente implemento las operaciones más comunes (++,*,->,etc) y no hago distinción entre Input y Output (todos mis       *
* iteradores son de lectura/escritura).										             *
*														             *
* La plantilla de los iteradores es mas sencilla (No especifico la categoría del iterador*, la distancia siempre será uint*, *
* y el tipo puntero siempre es T* y la referencia &T)  [Los asteriscos de iterador y uint son una referencia a la nota       *
* inferior, no punteros]												     *
*															     *
* *Si es necesario en un futuro igual me lo replanteo.									     *  
*****************************************************************************************************************************/

#ifndef __ITERATOR_H
#define __ITERATOR_H

#include "Base.h"

//Iterador base:
template<class T>
class TADiterator
{
public:
	virtual T& at()        throw(InvalidTADOperationException) = 0;//Aunque el diseño OO sea menos eficiente (cache misses), lo hago para hacer una jerarquía más clara
	T* operator->()        throw(InvalidTADOperationException) {return &at();}//Los iteradores son de lectura/escritura (Ver nota al principio)
	virtual T& operator*() throw(InvalidTADOperationException) {return at();}
};

//Iterador unidireccional (Positivo):
template<class T>
class ForwardIterator : public TADiterator<T>
{
public:
	virtual void operator++()            = 0;//Podría haber hecho una inline del estilo {return this+1;}, pero seguramente ++ sea de O(1) en la mayoría de los casos, y + O(n) en algunos (En general debería ser O(1)?)
	virtual void operator+=(uint offset) = 0;
};

//Iterador unidireccional (Negativo):
template<class T>
class ReverseIterator : public TADiterator<T>
{
public:
	virtual void operator--()            = 0;
	virtual void operator-=(uint offset) = 0;
};

//Iterador bidireccional:
template<class T>
class BidirectionalIterator : public TADiterator<T>
{
public:
	virtual void operator++() = 0;
	virtual void operator--() = 0;
	virtual void operator+=(uint offset) = 0;
	virtual void operator-=(uint offset) = 0;
};

//Objeto consulta:
template<class T>
class Query
{
public:
	virtual bool operator()(const T& data) = 0;
};

//Iterador de consulta:
template<class T>
class QueryIterator : public TADiterator<T>
{
protected:
	Query<T>* _query;//Puntero al objeto consulta
	QueryIterator()                : _query(NULL)  {}
	QueryIterator(Query<T>* Query) : _query(Query) {}
public:
	virtual void operator++() = 0;
};
#endif