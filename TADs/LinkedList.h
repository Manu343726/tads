/*****************************************************************
* LinkedList.h - Manuel Sánchez Pérez - Mar 2013		 *
*								 *
* Implementación de lista mediante una lista doblemente enlazada *
*                                                                *      
* A diferencia de su "hermana" List<T>, LinkedList<T> no permite *
* el acceso aleatorio, únicamente se puede acceder mediante      *
* iteradores.                                                    *       
*****************************************************************/

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "_LinkedContainer.h"
#include "Iterator.h"

template<class T>
class LinkedList : public _LinkedContainer<T>
{
public:
	class LinkedListIterator : public BidirectionalIterator<T>
	{
		friend class LinkedList<T>;
	protected:
		_LinkedListNode* _node;
		LinkedListIterator(_LinkedListNode* node) : _node(node) {}
	public:
		void operator++()
		{
			if(_node != NULL)
				_node = _node ->_next;
		}
		void operator--()
		{
			if(_node != NULL)
				_node = _node ->_previous;
		}

		void operator+=(uint offset) {*this = *this + offset;}
		void operator-=(uint offset) {*this = *this - offset;}

		LinkedListIterator operator+(uint offset) const
		{
			uint i = 0;
			_LinkedListNode* newNode = _node;

			while(i < offset && newNode!=NULL)
			{
				newNode = newNode->_next;
				++i;
			}

			return LinkedListIterator(newNode);
		}

		LinkedListIterator operator-(uint offset) const
		{
			uint i = 0;
			_LinkedListNode* newNode = _node;

			while(i < offset && newNode!=NULL)
			{
				newNode = newNode->_previous;
				++i;
			}

			return LinkedListIterator(newNode);
		}

		T& at() throw(InvalidTADOperationException)
		{
			if(_node != NULL)
				return _node->_data;
			else
				throw InvalidTADOperationException("LinkedListIterator::at() : NULL iterator");
		}

		bool operator==(const LinkedListIterator& other) const {return _node == other._node;}
		bool operator!=(const LinkedListIterator& other) const {return !(*this == other);}
	};

	class LinkedListQueryIterator : public QueryIterator<T>
	{
		friend class LinkedList<T>;
	protected:
		_LinkedListNode* _node;
		LinkedListQueryIterator(Query<T>* query,_LinkedListNode* node) : QueryIterator<T>(query) , _node(node) {}
	public:
		void operator++() throw(InvalidTADOperationException)
		{
			if(_query != NULL && _node != NULL)
			{
				_node = _node ->_next;

				while(_node != NULL && !(*_query)(at()))
					_node = _node->_next;
			}
		}

		T& at() throw(InvalidTADOperationException)
		{
			if(_node != NULL)
				return _node->_data;
			else
				throw InvalidTADOperationException("LinkedListQueryIterator::at() : NULL iterator");
		}

		bool operator==(const LinkedListQueryIterator& other) const {return _node == other._node;}
		bool operator!=(const LinkedListQueryIterator& other) const {return !(*this == other);}
	};

	void insert(const T& data,const LinkedListIterator& it) {return _insert(data, it._node);}
	void erase(const LinkedListIterator& it)                {return _insert(data, it._node);}
	void push_back(const T& data)                           {_push_back(data);}
	void pop_back()                                         {_pop_back();}
	void push_front(const T& data)                          {_push_front(data);}
	void pop_front()                                        {_pop_front();}

	const LinkedListIterator begin()                          const {return LinkedListIterator(_begin);}
	const LinkedListIterator end()                            const {return LinkedListIterator(NULL);}
	const LinkedListQueryIterator beginQuery(Query<T>* query) const {return LinkedListQueryIterator(query,_begin);}
	const LinkedListQueryIterator endQuery()                  const {return LinkedListQueryIterator(NULL,NULL);}

	const uint size()    const {return _Size();}
	const uint isEmpty() const {return _isEmpty();}
	void clear()               {_clear();}

	T& front() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _begin->_data;
		else
			throw InvalidTADOperationException("LinkedList<T>::front() : The list is empty");
	}
	T& back() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _end->_data;
		else
			throw InvalidTADOperationException("LinkedLsit<T>::back() : The list is empty");
	}
};

#endif