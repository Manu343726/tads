/****************************************************************
* LinkedQueue.h - Manuel Sánchez Pérez - Mar 2013	    	*
*							        *
* Implementación de cola mediante una lista doblemente enlazada *
****************************************************************/

#ifndef LINKED_QUEUE_H
#define LINKED_QUEUE_H

#include "_LinkedContainer.h"

template<class T>
class LinkedQueue : public _LinkedContainer<T>
{
public:
	void push_front(const T& data) {_push_front(data);}
	void pop_back()                {_pop_back();}

	T& front() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _begin->_data;
		else
			throw InvalidTADOperationException("LinkedQueue<T>::front() : The queue is empty");
	}
	T& back() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _end->_data;
		else
			throw InvalidTADOperationException("LinkedQueue<T>::back() : The queue is empty");
	}

	const uint size()    const {return _Size();}
	const bool isEmpty() const {return _isEmpty();}
	void clear()               {_clear();}
};

#endif