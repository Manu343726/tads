#ifndef ARRAY_H
#define ARRAY_H

#include "Base.h"

//El TAD más sencillo que se te puede ocurrir:
template<uint SIZE,class T>
class Array
{
private:
	T _array[SIZE];
public:
	Array() {}
	Array(const Array<SIZE,T>& other)
	{
		for(uint i=0;i<SIZE;++i)
			_array[i] = other._array[i];
	}

	//Constructor parametrizado: Inicializa los elementos del array a un valor especificado
	Array(const T& initValue)
	{
		for(uint i=0;i<SIZE;++i)
			_array[i] = initValue;
	}

	//Operador de indexación:
	T& operator[](uint index) throw (OutOfRangeException) //NOTA: El compilador de Visual Studio no implementa la especificación de excepciones, ésto genera el warning  C4290, pero lo pongo por claridad
	{
		if(index < SIZE)
			return _array[index];
		else
			throw OutOfRangeException(SIZE,index,"Array<SIZE,T>::operator[](uint index) : Parameter 'index' out of range");
	}
};

template<uint WIDTH, uint HEIGHT,class T>
class Matrix
{
private:
	Array<HEIGHT,T> _array[WIDTH];
public:
	Matrix() {}
	Matrix(const Matrix<WIDTH,HEIGHT,T>& other)
	{
		for(uint i=0;i<WIDTH;++i)
			for(uint j=0;j<HEIGHT;++j)
				_array[i][j] = other._array[i][j];
	}
	//Constructor parametrizado: Inicializa los elementos de la matriz a un valor especificado
	Matrix(const T& initValue)
	{
		for(uint i=0;i<WIDTH;++i)
			for(uint j=0;j<HEIGHT;++j)
				_array[i][j] = initValue;
	}

	//Operador de indexación:
	Array<WIDTH,T>& operator[](uint index) throw (OutOfRangeException) //NOTA: El compilador de Visual Studio no implementa la especificación de excepciones, ésto genera el warning  
	{
		if(index < WIDTH)
			return _array[index];
		else
			throw OutOfRangeException(WIDTH,index,"Matrix<WIDTH,HEIGHT,T>::operator[](uint index) : Parameter 'index' out of range");
	}
};
#endif