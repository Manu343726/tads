/*************************************************************************************
* TADs - Implementaci�n de TADs lineales											 *
* Manuel S�nchez P�rez, Mar 2013													 *
*																					 *
* Descripci�n de las cabeceras:														 *
*  - Base.h: Declaraciones de tipos b�sicos y herramientas							 *
*  - Iterator.h: Jerarqu�a de iteradores. Iteradores base							 *
*  - _linearContainer.h: Contenedor gen�rico (array din�mico)						 *
*  - _linkedContainer.h: Contenedor gen�rico (Lista doblemente enlazada)			 *
*  - List.h: TAD lista (Implementaci�n con array din�mico)							 *
*  - Stack.h: TAD pila (Implementaci�n con array din�mico)							 *
*  - LinkedList.h: TAD lista (Implementaci�n con lista doblemente enlazada)          *
*  - LinkedStack.h: TAD pila (Implementaci�n con lista doblemente enlazada)			 *
*  - LinkedQueue.h: TAD cola (Implementaci�n con lista doblemente enlazada)			 *
*																					 *
* �ste archivo contiene todas las cabeceras, en el orden en el que fueron descritas. *
* Las cabeceras originales pueden obtenerse en https://github.com/Manu343726/TADs    *
*************************************************************************************/




/*******************************************************************************************************************************************
* Base.h - Manuel S�nchez P�rez	- Mar 2013																				                   *
*																															               *
* �ste archivo contiene las definiciones de los tipos b�sicos utilizados por los TADs, as� como algunas herramientas.	                   *
*                                                                                                                                          *
* La secci�n "Herramientas varias" est� sacada del archivo de configuraci�n del proyecto dx_lib32 (https://github.com/Manu343726/dx_lib32) *
*******************************************************************************************************************************************/

#ifndef BASE_H
#define BASE_H

#ifdef _MSC_VER
#pragma warning(disable : 4290) //Visual Studio no implementa la especificaci�n de excepciones. A cambio te lanza el warning 4290 (http://msdn.microsoft.com/en-us/library/sa28fef8.aspx)
#endif

#include <exception>
using namespace std;

typedef unsigned int uint; //M�s sencillo de escribir...

//Definimos un tipo "rango", que utilizaremos para guardar informaci�n sobre buffer overflows, entre otras cosas:
struct Range
{
private:
	uint _begin;
	uint _end;
public:
	//CONSTRUCTORES:
	Range()                     {_begin=0;_end=0;}
	Range(uint begin, uint end) {_begin = begin; _end = end;}
	Range(uint size)            {_begin=0;_end = size-1;}

	//OBSERVADORES:
	uint begin()              const {return _begin;}
	uint end()                const {return _end;}
	bool contains(uint value) const {return _begin <= value && value <= _end;}

	//OPERADORES:
	bool operator==(Range other) {return _begin == other._begin && _end == other._end;}
	bool operator!=(Range other) {return !(*this == other);}
};

#define EXCEPTION_DEFAULT_MESSAGE(className) "Unexpected " #className

typedef exception TADException;//Deber�a ser ADTException, pero lo dejo as� para seguir la notaci�n de la asignatura
typedef TADException InvalidTADOperationException;
typedef InvalidTADOperationException IncomparableException;


//Out of range exception:
class OutOfRangeException : public InvalidTADOperationException
{
private:
	Range _range;
	uint _value;
public:
	OutOfRangeException(Range range, uint value, char* message = EXCEPTION_DEFAULT_MESSAGE(OutOfRangeException)) : InvalidTADOperationException(message) {_range = range; _value = value;}
	OutOfRangeException(uint size, uint value, char* message = EXCEPTION_DEFAULT_MESSAGE(OutOfRangeException))   : InvalidTADOperationException(message) , _range(size) {_value = value;}

	const uint value()  const {return _value;}
	const Range range() const {return _range;}
};

/******************************
*     HERRAMIENTAS VARIAS     *
******************************/

#define SAFEDELETE_POINTER(x) {delete (x); (x)=NULL;}
#define SAFEDELETE_ARRAY(x) {delete[] (x); (x)=NULL;}


//Es una parida, lo se, pero para configurar a base de preprocesador es mas c�modo (El c�digo queda mas claro):
#ifndef TRUE
	#define TRUE 1
	#define FALSE 0
#endif

#define ANDOP(a,b) ((a == TRUE && b == TRUE) ? TRUE : FALSE) 
#define OROP(a,b) ((a == TRUE || b == TRUE) ? TRUE : FALSE) 
#define NOTOP(a) ((a == TRUE) ? FALSE : TRUE)

#ifndef YES
	#define YES TRUE
	#define NO FALSE
#endif
#ifndef ENABLED
	#define ENABLED TRUE
	#define DISABLED FALSE
#endif

//Por si se da el caso de usar floats: 
#define FLOAT_EPSILON 0.0001

#define FLOAT_EQ(x,y) (((y - FLOAT_EPSILON) < x) && ((y + FLOAT_EPSILON) > x))
#define FLOAT_NE(x,y) !FLOAT_EQ(x,y)
#define FLOAT_EQCERO(x) ((x < FLOAT_EPSILON) && (x > -FLOAT_EPSILON))
#define FLOAT_NECERO(x) !FLOAT_EQCERO(x)

#define FLOAT_GE(x,y) (FLOAT_EQ(x,y) || (x > y))
#define FLOAT_LE(x,y) (FLOAT_EQ(x,y) || (x < y))
#endif




/*****************************************************************************************************************************
* Iterator.h - Manuel S�nchez P�rez	- Mar 2013																				 *
*																															 *
* �ste archivo contiene las definiciones de las diferentes categor�as de iteradores.										 *
* Es una versi�n simplificada de la clasificaci�n de iteradores de la STL (http://www.cplusplus.com/reference/iterator/).	 *
* �nicamente implemento las operaciones m�s comunes (++,*,->,etc) y no hago distinci�n entre Input y Output (todos mis       *
* iteradores son de lectura/escritura).																						 *
*																															 *
* La plantilla de los iteradores es mas sencilla (No especifico la categor�a del iterador*, la distancia siempre ser� uint*, *
* y el tipo puntero siempre es T* y la referencia &T)  [Los asteriscos de iterador y uint son una referencia a la nota       *
* inferior, no punteros]																									 *
*																															 *
* *Si es necesario en un futuro igual me lo replanteo.																	     *
*****************************************************************************************************************************/

#ifndef __ITERATOR_H
#define __ITERATOR_H

#include "Base.h"

//Iterador base:
template<class T>
class TADiterator
{
public:
	virtual T& at()        throw(InvalidTADOperationException) = NULL;//Aunque el dise�o OO sea menos eficiente (cache misses), lo hago para hacer una jerarqu�a m�s clara
	T* operator->()        throw(InvalidTADOperationException) {return &at();}//Los iteradores son de lectura/escritura (Ver nota al principio)
	virtual T& operator*() throw(InvalidTADOperationException) {return at();}
};

//Iterador unidireccional (Positivo):
template<class T>
class ForwardIterator : public TADiterator<T>
{
public:
	virtual void operator++()            = NULL;//Podr�a haber hecho una inline del estilo {return this+1;}, pero seguramente ++ sea de O(1) en la mayor�a de los casos, y + O(n) en algunos (En general deber�a ser O(1)?)
	virtual void operator+=(uint offset) = NULL;
};

//Iterador unidireccional (Negativo):
template<class T>
class ReverseIterator : public TADiterator<T>
{
public:
	virtual void operator--()            = NULL;
	virtual void operator-=(uint offset) = NULL;
};

//Iterador bidireccional:
template<class T>
class BidirectionalIterator : public TADiterator<T>
{
public:
	virtual void operator++() = NULL;
	virtual void operator--() = NULL;
	virtual void operator+=(uint offset) = NULL;
	virtual void operator-=(uint offset) = NULL;
};

//Objeto consulta:
template<class T>
class Query
{
public:
	virtual bool operator()(const T& data) = NULL;
};

//Iterador de consulta:
template<class T>
class QueryIterator : public TADiterator<T>
{
protected:
	Query<T>* _query;//Puntero al objeto consulta
	QueryIterator()                : _query(NULL)  {}
	QueryIterator(Query<T>* Query) : _query(Query) {}
public:
	virtual void operator++() = NULL;
};
#endif;




/************************************************************************
* _LinearContainer.h - Manuel S�nchez P�rez	- Mar 2013					*
*																		*
* La plantilla _LinkedContainer<T> provee la implementaci�n completa de *
* un array din�mico, para ser utilizado en diferentes TADs.             *
************************************************************************/

#ifndef LINEARCONTAINER_H
#define LINEARCONTAINER_H
#include "Base.h"

const int INITALLOCSIZE = 10;
const float EXPANDFACTOR = 2.0f; 
const float CONTRACTFACTOR = 0.5f;

//Contenedor secuencial de datos:
template<class T>
class _LinearContainer
{
protected:
	T* _array;
	uint _size;
	uint _count;
	
	void _resize(uint newSize)
	{
		uint minSize = _count < newSize ? _count : newSize;
		T* newArray = new T[newSize];

		for(uint i=0;i<minSize;++i)
			newArray[i] = _array[i];

		delete [] _array;
		_array = newArray;
		_size = newSize;
	}

	void _clear()
	{
		_resize(INITALLOCSIZE);
		_count = 0;
	}

	void _rightShift(uint index)
	{
		for(uint i= _count;i>index;--i)
			_array[i] = _array[i-1];
	}

	void _leftShift(uint index)
	{
		for(uint i = index;i<_count - 1; ++i)
			_array[i] = _array[i+1];
	}

	void _insert(const T& value, uint index) throw(OutOfRangeException)
	{
		if(index <= _count)
		{
			if(_count == _size)
				_resize((uint)(_size*EXPANDFACTOR));

			_rightShift(index);
			_array[index] = value;
			_count++;
		}
		else
			throw OutOfRangeException(_count+1,index);
	}

	void _erase(uint index) throw(OutOfRangeException)
	{
		if(index < _count)
		{
			if(_size > INITALLOCSIZE && _count <= (uint)(_size*CONTRACTFACTOR))
				_resize((uint)(_size*CONTRACTFACTOR));

			_leftShift(index);
			_count--;
		}
		else
			throw OutOfRangeException(_count,index);
	}

	void _push_back(const T& value)  throw(OutOfRangeException) {return _insert(value,_count);}
	void _push_front(const T& value) throw(OutOfRangeException) {return _insert(value,0);}
	void _pop_back()                 throw(OutOfRangeException) {return _erase(_count-1);}
	void _pop_front()                throw(OutOfRangeException) {return _erase(0);}

	const uint _Size()    const {return _count;} //Ojo, es el tama�o del contenedor, no de su implementaci�n (Al usuario no le interesa como lo has implementado)
	const bool _isEmpty() const {return _count == 0;}


	//Constructor protected: �sta clase solo es "instanciable" por sus clases derivadas, el usuario no puede utilizarla 
	_LinearContainer()
	{
		_array = new T[INITALLOCSIZE];
		_count = 0;
		_size = INITALLOCSIZE;
	}

	~_LinearContainer()
	{
		SAFEDELETE_ARRAY(_array);
	}
};
#endif




/************************************************************************
* _LinkedContainer.h - Manuel S�nchez P�rez	- Mar 2013					*
*																		*
* La plantilla _LinkedContainer<T> provee la implementaci�n completa de *
* una lista doblemente enlazada, para ser utilizada en                  *
* diferentes TADs.                                                      *
************************************************************************/

#ifndef _LinkedContainer_H
#define _LinkedContainer_H

#include "Base.h"

template<class T>
class _LinkedContainer
{
protected:
	class _LinkedListNode
	{
	public:
		T _data;
		_LinkedListNode* _next;
		_LinkedListNode* _previous;

		_LinkedListNode()                                                              : _next(NULL)                                     {}
		_LinkedListNode(const T& data)                                                 : _next(NULL) , _data(data)                       {}
		_LinkedListNode(const T& data,_LinkedListNode* previous,_LinkedListNode* next) : _next(next) , _previous(previous) , _data(data) {}
		_LinkedListNode(_LinkedListNode* previous)                                     : _next(NULL) , _previous(previous)               {} //Para el constructor del iterador end()
	};

	_LinkedListNode* _begin;
	_LinkedListNode* _end;
	uint _count;

	_LinkedContainer() : _begin(NULL) , _end(NULL) , _count(0) {}
	~_LinkedContainer()
	{
		_clear();
	}

	void _clear()
	{
		if(_count > 0)
		{
			_LinkedListNode* next = _begin->_next;

			while(next != _end->_next)
			{
				delete _begin;
				_begin = next;
				next = next->_next;
			}

			_begin = NULL;
			_end = NULL;
			_count = 0;
		}
	}

	void _push_front(const T& data)
	{
		_LinkedListNode* newNode = new _LinkedListNode(data,NULL,_begin);

		if(_begin != NULL)
			_begin->_previous = newNode;

		_begin = newNode;

		if(_end == NULL)
			_end = _begin;

		_count++;
	}

	void _push_back(const T& data)
	{
		_LinkedListNode* newNode = new _LinkedListNode(data,_end,NULL);

		if(_end != NULL)
			_end->_next = newNode;

		_end = newNode;

		if(_begin == NULL)
			_begin = _end;

		_count++;
	}

	void _pop_front()
	{
		if(_begin != NULL)
		{
			_LinkedListNode* newBegin = _begin->_next;

			if(newBegin != NULL)
				newBegin->_previous = NULL;

			delete _begin;

			_begin = newBegin;
			if(_end == NULL)
				_begin = NULL;
			_count--;
		}
	}

	void _pop_back()
	{
		if(_end != NULL)
		{
			_LinkedListNode* newEnd = _end->_previous;

			if(newEnd != NULL)
				newEnd->_next = NULL;

			delete _end;

			_end = newEnd;
			if(_end == NULL)
				_begin = NULL;

			_count--;
		}
	}

	void _insert(const T& data, _LinkedListNode* node)
	{
		if(node == _begin)
			_push_front(data);
		else if(node == _end)
			_push_back(data);
		else
		{
			_stc_insert(data,node);
			_count++;
		}
	}

	void _erase(_LinkedListNode * node)
	{
		if(node == _begin)
			_pop_front();
		else if(node == _end)
			_pop_back();
		else
		{
			_stc_erase(node);
			_count--;
		}
	}

	const uint _Size()    const {return _count;} //Ojo, es el tama�o del contenedor, no de su implementaci�n (Al usuario no le interesa como lo has implementado)
	const bool _isEmpty() const {return _begin == NULL;}
private:
	static _LinkedListNode* _stc_insert(const T& data, _LinkedListNode* node)
	{
		_LinkedListNode* newNode = new _LinkedListNode(data,node->_previous,node);

		if(node->_previous != NULL)
			node->_previous->_next = newNode;
		if(node->_next != NULL)
			node->_next->_previous = newNode;

		return newNode;
	}

	static void _stc_erase(_LinkedListNode* node) throw(InvalidTADOperationException)
	{
		if(node != NULL)
		{
			_LinkedListNode* next = node->_next;
			_LinkedListNode* previous = node->_previous;

			if(next != NULL)
				next->_previous = previous;
			if(previous != NULL)
				previous->_next = next;

			delete node;
		}
		else
			throw InvalidTADOperationException();
	}
};
#endif




/*****************************************************
* List.h - Manuel S�nchez P�rez - Mar 2013	         *
*												     *
* Implementaci�n de lista mediante un array din�mico *
*****************************************************/

#ifndef LIST_H
#define LIST_H

#include "_LinearContainer.h"
#include "Iterator.h"

template<class T>
class List : public _LinearContainer<T>
{
	friend class ListIterator;
public:
	class ListIterator : public BidirectionalIterator<T>
	{
		friend class List<T>;
	private:
		uint _index;
		T* _containerArray;
		uint _containerSize;

		ListIterator(uint index,uint size,T* array) {_index = index;_containerSize = size; _containerArray = array;}
	public:
		void operator++() {_index++;}
		void operator--() {_index--;}
		void operator+=(uint offset) {_index+=offset;}
		void operator-=(uint offset) {_index-=offset;}

		ListIterator operator+(uint offset) const {return ListIterator(_index + offset,_containerSize,_containerArray);} //No es necesario retorno por referencia (RVO)
		ListIterator operator-(uint offset) const {return ListIterator(_index - offset,_containerSize,_containerArray);} //No es necesario retorno por referencia (RVO)

		T& at() throw(OutOfRangeException)
		{
			if(_index<_containerSize)
				return _containerArray[_index];
			else
				throw OutOfRangeException(_containerSize,_index,"ListIterator::at() : Invalid iterator");
		}

		bool operator==(const ListIterator& other) const {return _index == other._index && _containerArray == other._containerArray;}
		bool operator!=(const ListIterator& other) const {return !(*this == other);}

		bool operator>(const ListIterator& other) throw(IncomparableException)
		{
			if(_containerArray == other._containerArray)
				return _index > other._index;
			else
				throw IncomparableException("ListIterator::operator>() : Iterators not come from the same container");
		}
		bool operator>=(const ListIterator& other) throw(IncomparableException)
		{
			if(_containerArray == other._containerArray)
				return _index >= other._index;
			else
				throw IncomparableException("ListIterator::operator>=() : Iterators not come from the same container");
		}
		bool operator<(const ListIterator& other) throw(IncomparableException)
		{
			if(_containerArray == other._containerArray)
				return _index < other._index;
			else
				throw IncomparableException("ListIterator::operator<() : Iterators not come from the same container");
		}
		bool operator<=(const ListIterator& other) throw(IncomparableException)
		{
			if(_containerArray == other._containerArray)
				return _index <= other._index;
			else
				throw IncomparableException("ListIterator::operator<=() : Iterators not come from the same container");
		}
	};

	class ListQueryIterator : public QueryIterator<T>
	{
		friend class List<T>;
	private:
		uint _index;
		T* _containerArray;
		uint _containerSize;
	
		ListQueryIterator(Query<T>* Query, uint index, uint containerSize, T* containerArray) : QueryIterator<T>(Query) , _index(index) , _containerArray(containerArray) , _containerSize(containerSize) {}
	public:
		T& at() throw(OutOfRangeException)
		{
			if(_index<_containerSize)
				return _containerArray[_index];
			else
				throw OutOfRangeException(_containerSize,_index,"ListIterator::at() : Invalid iterator");
		}

		void operator++() throw(OutOfRangeException)
		{
			if(_query != NULL)
			{
				_index++;

				while(_index < _containerSize && !(*_query)(at()))
					_index++;
			}
		}

		bool operator==(const ListQueryIterator& other) const {return _index == other._index && _containerArray == other._containerArray;}
		bool operator!=(const ListQueryIterator& other) const {return !(*this == other);}
	};

	List(){};//Lo hace todo la clase base
	~List(){};//Lo hace todo la clase base

	void insert(const T& value, uint index)              throw(OutOfRangeException) {_insert(value,index);}
	void insert(const T& value,const ListIterator& iter) throw(OutOfRangeException) {_insert(value,iter._index);}
	void erase(uint index)                               throw(OutOfRangeException) {_insert(index);}
	void erase(const ListIterator& iter)                 throw(OutOfRangeException) {_erase(iter._index);}
	void push_back(const T& value)                       throw(OutOfRangeException) {_push_back(value);}
	void pop_back()                                      throw(OutOfRangeException) {_pop_back();}

	const uint size()    const {return _Size();}
	const bool isEmpty() const {return _isEmpty();}
	void clear()               {_clear();}

	const ListIterator begin() {return ListIterator(0,_count,_array);} //No hace falta que el retorno sea por referencia (RVO)
	const ListIterator end()   {return ListIterator(_count,_count,_array);} //No hace falta que el retorno sea por referencia (RVO)

	const ListQueryIterator beginQuery(Query<T>* Query) {return ListQueryIterator(Query,0,_count,_array);}
	const ListQueryIterator endQuery()                  {return ListQueryIterator(NULL,_count,_count,_array);}

	T& at(uint index) throw(OutOfRangeException)
	{
		if(index < _count)
			return _array[index];
		else
			throw OutOfRangeException(_count,index,"List<T>::at() : 'index' is out of range");
	}
	T& operator[](uint index) throw(OutOfRangeException) {return at(index);}

	T& front() throw(OutOfRangeException) {return at(0);}
	T& back()  throw(OutOfRangeException) {return at(_count-1);}
};
#endif




/**************************************************************
* Stack.h - Manuel S�nchez P�rez - Mar 2013	            	  *
*														      *
* Implementaci�n de pila mediante un array din�mico           *
*												     	      *
* NOTA: La implementaci�n es id�ntica a la de LinkedStack<T>, *
*  �nicamente se diferencian por el contenedor utilizado.     *
**************************************************************/

#ifndef STACK_H
#define STACK_H

#include "_LinearContainer.h"

template<class T>
class Stack : public _LinearContainer<T>
{
public:
	void push_back(const T& data)  {_push_back(data);}
	void pop_back()   {_pop_back();}

	T& back() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _end->_data;
		else
			throw InvalidTADOperationException("Stack<T>::back() : The stack is empty");
	}

	const uint size()    const {return _Size();}
	const bool isEmpty() const {return _isEmpty();}
	void clear()               {_clear();}
};

#endif




/*****************************************************************
* LinkedList.h - Manuel S�nchez P�rez - Mar 2013				 *
*																 *
* Implementaci�n de lista mediante una lista doblemente enlazada *
*																 *
* A diferencia de su "hermana" List<T>, LinkedList<T> no permite *
* el acceso aleatorio, �nicamente se puede acceder mediante      *
* iteradores.                                                    *       
*****************************************************************/

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "_LinkedContainer.h"
#include "Iterator.h"

template<class T>
class LinkedList : public _LinkedContainer<T>
{
public:
	class LinkedListIterator : public BidirectionalIterator<T>
	{
		friend class LinkedList<T>;
	protected:
		_LinkedListNode* _node;
		LinkedListIterator(_LinkedListNode* node) : _node(node) {}
	public:
		void operator++()
		{
			if(_node != NULL)
				_node = _node ->_next;
		}
		void operator--()
		{
			if(_node != NULL)
				_node = _node ->_previous;
		}

		void operator+=(uint offset) {*this = *this + offset;}
		void operator-=(uint offset) {*this = *this - offset;}

		LinkedListIterator operator+(uint offset) const
		{
			uint i = 0;
			_LinkedListNode* newNode = _node;

			while(i < offset && newNode!=NULL)
			{
				newNode = newNode->_next;
				++i;
			}

			return LinkedListIterator(newNode);
		}

		LinkedListIterator operator-(uint offset) const
		{
			uint i = 0;
			_LinkedListNode* newNode = _node;

			while(i < offset && newNode!=NULL)
			{
				newNode = newNode->_previous;
				++i;
			}

			return LinkedListIterator(newNode);
		}

		T& at() throw(InvalidTADOperationException)
		{
			if(_node != NULL)
				return _node->_data;
			else
				throw InvalidTADOperationException("LinkedListIterator::at() : NULL iterator");
		}

		bool operator==(const LinkedListIterator& other) const {return _node == other._node;}
		bool operator!=(const LinkedListIterator& other) const {return !(*this == other);}
	};

	class LinkedListQueryIterator : public QueryIterator<T>
	{
		friend class LinkedList<T>;
	protected:
		_LinkedListNode* _node;
		LinkedListQueryIterator(Query<T>* query,_LinkedListNode* node) : QueryIterator<T>(query) , _node(node) {}
	public:
		void operator++() throw(InvalidTADOperationException)
		{
			if(_query != NULL && _node != NULL)
			{
				_node = _node ->_next;

				while(_node != NULL && !(*_query)(at()))
					_node = _node->_next;
			}
		}

		T& at() throw(InvalidTADOperationException)
		{
			if(_node != NULL)
				return _node->_data;
			else
				throw InvalidTADOperationException("LinkedListQueryIterator::at() : NULL iterator");
		}

		bool operator==(const LinkedListQueryIterator& other) const {return _node == other._node;}
		bool operator!=(const LinkedListQueryIterator& other) const {return !(*this == other);}
	};

	void insert(const T& data,const LinkedListIterator& it) {return _insert(data, it._node);}
	void erase(const LinkedListIterator& it)                {return _insert(data, it._node);}
	void push_back(const T& data)                           {_push_back(data);}
	void pop_back()                                         {_pop_back();}
	void push_front(const T& data)                          {_push_front(data);}
	void pop_front()                                        {_pop_front();}

	const LinkedListIterator begin()                          const {return LinkedListIterator(_begin);}
	const LinkedListIterator end()                            const {return LinkedListIterator(NULL);}
	const LinkedListQueryIterator beginQuery(Query<T>* query) const {return LinkedListQueryIterator(query,_begin);}
	const LinkedListQueryIterator endQuery()                  const {return LinkedListQueryIterator(NULL,NULL);}

	const uint size()    const {return _Size();}
	const uint isEmpty() const {return _isEmpty();}
	void clear()               {_clear();}

	T& front() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _begin->_data;
		else
			throw InvalidTADOperationException("LinkedList<T>::front() : The list is empty");
	}
	T& back() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _end->_data;
		else
			throw InvalidTADOperationException("LinkedLsit<T>::back() : The list is empty");
	}
};

#endif




/*******************************************************************
* LinkedStack.h - Manuel S�nchez P�rez - Mar 2013				   *
*																   *
* Implementaci�n de pila mediante una lista doblemente enlazada    *
*																   *
* NOTA: La implementaci�n es id�ntica a la de Stack<T>, �nicamente *
* se diferencian por el contenedor utilizado.                      *
*******************************************************************/

#ifndef LINKED_STACK_H
#define LINKED_STACK_H

#include "_LinkedContainer.h"

template<class T>
class LinkedStack : public _LinkedContainer<T>
{
public:
	void push_back(const T& data)  {_push_back(data);}
	void pop_back()                {_pop_back();}

	T& back() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _end->_data;
		else
			throw InvalidTADOperationException("LinkedStack<T>::back() : The stack is empty");
	}

	const uint size()    const {return _Size();}
	const bool isEmpty() const {return _isEmpty();}
	void clear()               {_clear();}
};

#endif




/****************************************************************
* LinkedQueue.h - Manuel S�nchez P�rez - Mar 2013		    	*
*														        *
* Implementaci�n de cola mediante una lista doblemente enlazada *
****************************************************************/

#ifndef LINKED_QUEUE_H
#define LINKED_QUEUE_H

#include "_LinkedContainer.h"

template<class T>
class LinkedQueue : public _LinkedContainer<T>
{
public:
	void push_front(const T& data) {_push_front(data);}
	void pop_back()                {_pop_back();}

	T& front() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _begin->_data;
		else
			throw InvalidTADOperationException("LinkedQueue<T>::front() : The queue is empty");
	}
	T& back() throw (InvalidTADOperationException)
	{
		if(!_isEmpty())
			return _end->_data;
		else
			throw InvalidTADOperationException("LinkedQueue<T>::back() : The queue is empty");
	}

	const uint size()    const {return _Size();}
	const bool isEmpty() const {return _isEmpty();}
	void clear()               {_clear();}
};

#endif