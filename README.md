TADs - Implementación de TADs lineales
======================================											 
																					 
Descripción de las cabeceras:
-----------------------------
  - **Base.h**: Declaraciones de tipos básicos y herramientas							 
  - **Iterator.h**: Jerarquía de iteradores. Iteradores base							 
  - **_linearContainer.h**: Contenedor genérico (array dinámico)						 
  - **_linkedContainer.h**: Contenedor genérico (Lista doblemente enlazada)			 
  - **List.h**: TAD lista (Implementación con array dinámico)							 
  - **Stack.h**: TAD pila (Implementación con array dinámico)							 
  - **LinkedList.h**: TAD lista (Implementación con lista doblemente enlazada)          
  - **LinkedStack.h**: TAD pila (Implementación con lista doblemente enlazada)			 
  - **LinkedQueue.h**: TAD cola (Implementación con lista doblemente enlazada)			 
																					 
En el directorio raiz del repo se incluye el archivo "TADs.h", una compilación de todas las cabeceras. 

